# **README** <!-- omit in toc -->
# Data Lakes <!-- omit in toc -->

<div align="center">
  <img width="300" src="img/001_data_lake_aws.png"/>
</div>
<br/><br/> <!-- Blank line -->

Model user activity data to create a Data Lake and **ETL**
(**Extract, Transform, Load**) pipeline for a music streaming app and
defining an On-Read-Schema to make an **ELT** process.

## **Table of Content** <!-- omit in toc -->

- [Introduction](#introduction)
- [Project Description](#project-description)
- [Project Development](#project-development)
  - [Datasets](#datasets)
    - [Song Dataset](#song-dataset)
    - [Log Dataset](#log-dataset)
      - [JSON Path](#json-path)
    - [Entity Relational Diagram (ERD)](#entity-relational-diagram-erd)
  - [Development Files](#development-files)
  - [Setup Development Environment](#setup-development-environment)
    - [Create Environment](#create-environment)
    - [Create Jupyter Kernel Environment](#create-jupyter-kernel-environment)
    - [Package Requirements](#package-requirements)
  - [Setup AWS](#setup-aws)
    - [Create IAM Role](#create-iam-role)
    - [Create Security Group](#create-security-group)
    - [Create Redshift Cluster Subnet Group](#create-redshift-cluster-subnet-group)
    - [Create Redshift Cluster](#create-redshift-cluster)
    - [Create IAM user with code permissions](#create-iam-user-with-code-permissions)
    - [Delete Redshift Cluster](#delete-redshift-cluster)
- [Pipeline Execution](#pipeline-execution)
- [Expected Results](#expected-results)
  - [Artist Dimension Table](#artist-dimension-table)
  - [Song Dimension Table](#song-dimension-table)
  - [Users Dimension Table](#users-dimension-table)
  - [Time Dimension Table](#time-dimension-table)
  - [Songplays Fact Table](#songplays-fact-table)
- [Discussion](#discussion)

<br/><br/> <!-- Blank line -->

# Introduction

A music streaming startup, _**Sparkify**_, has grown their user base and
song database and want to move their **Data Warehouse** to a
**Data Lake**.

Their data resides in **S3**, in a directory of `JSON` logs on user
activity on the app, as well as a directory with `JSON` metadata of the
songs in their app.

As a Data Engineer, build an **ETL pipeline** that extracts the data
from **S3**, processes them using **Spark**, and loads the data back
into **S3** as a set of dimensional tables. This will allow their
analytics team to continue finding insights in what songs their users
are listening to.

Test the database and ETL pipeline by running queries given by the
analytics team from _**Sparkify**_ and compare the results with the
expected outputs.
<br/><br/> <!-- Blank line -->

# Project Description

In the project is applied knowledge about **Data Lakes** and
**Spark** to build an **ETL pipeline** for a Data Lake hosted on
**S3**. To complete the project, it is required to load data from **S3**
and process the data into analytics tables using Spark, finally load
them back into S3, deploying this Spark process on a cluster using AWS.
<br/><br/> <!-- Blank line -->

# Project Development

To develop all the project, it is already a defined Schema, but no the
complete database structure, also, the raw data is only available in
`JSON` files, so a ETL pipeline is required and also an ELT process, the
following sections describe how the project is handled.
<br/><br/> <!-- Blank line -->

## Datasets

To understand the requirements, business purposes and technical
characteristics of the project, the datasets are located in **S3**, the
S3 links are the following ones, remember they are located in
`us-east-1` region (**Virginia/Oregon**):

- **Song data**: `s3://udacity-dend/song_data`
- **Long data**: `s3://udacity-dend/log_data`
- **Log Data JSON path**: `s3://udacity-dend/log_json_path.json`
<br/><br/> <!-- Blank line -->

### Song Dataset

Subset of real data from the
[**Million Song Dataset**](https://labrosa.ee.columbia.edu/millionsong/)
. Each file is in `JSON` format and contains _metadata_ about a song and
the artist of that song. The files are partitioned by the first three
letters of each song's track ID. Below are the root tree directory and
file path examples of two files in the dataset.

The tree below shows the subfolders are from `A/A/A` to `A/Z/Z`.
<br/><br/> <!-- Blank line -->

```bash
s3://udacity-dend/song_data
└───A
    ├────A
    │    ├────A
    │    ├────B
    │    ├────C
    │    ├────D
    │    ├────E
    │    ├────F
    │    ├────G
    │    ├────H
    │    ├────I
    │    ├────J
    │    ├────K
    │    ├────L
    │    ├────M
    │    ├────N
    │    ├────O
    │    ├────P
    │    ├────Q
    │    ├────R
    │    ├────S
    │    ├────T
    │    ├────U
    │    ├────V
    │    ├────W
    │    ├────X
    │    ├────Y
    │    └────Z
    ├────B
    │    ├────A
    │    ├────B
    │    ├────C
    │    ├────...
    │    └────Z
    ├────C
    │    ├────A
    │    ├────B
    │    ├────C
    │    ├────...
    │    └────Z
    ├────...
    └────Z
         ├────A
         ├────B
         ├────C
         ├────...
         └────Z
```

<br/><br/> <!-- Blank line -->

So, using the tree, it is possible to identify the different `JSON`
files available like the listed below:

- `s3://udacity-dend/song_data/A/A/A/TRAAAYL128F4271A5B.json`
- `s3://udacity-dend/song_data/A/Z/Z/TRAZZXH128F933D2BA.json`

Below is an example of what a single song file looks like
(`TRAAAYL128F4271A5B.json`):
<br/><br/> <!-- Blank line -->

```python
{
    "artist_id": "ARY589G1187B9A9F4E",
    "artist_latitude": 45.51179,
    "artist_location": "Portland, OR",
    "artist_longitude": -122.67563,
    "artist_name": "Talkdemonic",
    "duration": 171.57179,
    "num_songs": 1,
    "song_id": "SOIGHOD12A8C13B5A1",
    "title": "Indian Angel",
    "year": 2004
}
```

<br/><br/> <!-- Blank line -->

It is important to identify what is the content of the available files,
sometimes, the name of the attributes helps to know the type of data to
store, for example, the `duration` attribute and current value indicates
that is not possible to have sentences here, so at defining an
On-Read-Schema (if required) a good type of data could be `float`.

It is a recommendation to **visualize more than one** `JSON` file, it
helps to understand data better and identify possible inconsistencies or
common missing values before implementing a pipeline based on Spark and
reduce the probability of an bug appearance.
<br/><br/> <!-- Blank line -->

### Log Dataset

Consists of **log files** in `JSON` format generated by this
[event simulator](https://github.com/Interana/eventsim) based on the
songs in the dataset songs. These simulate activity logs from a music
streaming app based on specified configurations.

Below are the root tree directory and file path examples of two files in
the dataset, the log files in the dataset are partitioned by year and
month.
<br/><br/> <!-- Blank line -->

```bash
s3://udacity-dend/log_data
├───log_data
    └───2018
        └───11
```

<br/><br/> <!-- Blank line -->

- `s3://udacity-dend/log_data/2018/11/2018-11-01-events.json`
- `s3://udacity-dend/log_data/2018/11/2018-11-30-events.json`

The following image shows an example of what the data in a log file
(`2018-11-18-events.json`) looks like.

<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="800" src="img/002_event_data_sample.png" />
  <figcaption>Event sample data</figcaption>
</div>
<br/><br/> <!-- Blank line -->

As mentioned in the previous section, it is important to identify what
are the content of the available files, the name of the attributes helps
to know the type of data to store, for example, the `page` attribute
and values indicates that is possible to have only specific values and
no numbers, just strings, so at designing the database a good type of
data could be `varchar` or `text`.

It is a recommendation to **visualize more than one** `JSON` file, it
helps to understand data better and identify possible inconsistencies or
common missing values.

#### JSON Path

The following structure is included in the
`s3://udacity-dend/log_json_path.json` file, it is helpful to identify
the keys distribution from the JSON Logs.

The following snip code indicates its key distribution, this will also
helpful to copy the JSON files from S3 to Redshift.

```javascript
{
  "jsonpaths": [
    "$['artist']",
    "$['auth']",
    "$['firstName']",
    "$['gender']",
    "$['itemInSession']",
    "$['lastName']",
    "$['length']",
    "$['level']",
    "$['location']",
    "$['method']",
    "$['page']",
    "$['registration']",
    "$['sessionId']",
    "$['song']",
    "$['status']",
    "$['ts']",
    "$['userAgent']",
    "$['userId']"
  ]
}
```

> **Comments**:
>  
> - Use this
>   [JSON file format (video)](https://www.youtube.com/watch?time_continue=1&v=hO2CayzZBoA).
>   resource to better understand the JSON files.
> - **HINT**: Use the `value_counts` method on log dataframes, this is a
>   good option to identify attributes that store only specific values,
>   e.g:
>  
>     ```python
>           df["attribute_n"].value_counts()
>     ```
>  
> <br/><br/> <!-- Blank line -->
>  
<br/><br/> <!-- Blank line -->

### Entity Relational Diagram (ERD)

The `Star Schema` defined here helps to visualize the data and relations
between them (designed from previous projects on this Nanodegree; after
seeing some **songs** and **logs** `JSON` files, the following ERD
resulted, it includes a pair of tables working as **staging** storage,
here, only helps to remember the `JSON` On-Read_Schema to handle the
data using **Spark**.

<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="800" src="img/003_erd_reference.png"/>
  <figcaption>
    The Sparkify's Entity Relational Diagram (Just as reference)
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->

> **Comments**:
>  
> During the development, some attributes changed, it is ok, it is
> not possible to check all the files, and sometimes some data structure
> is not identified until the development code process.
>  
> <br/><br/>
>  

<br/><br/> <!-- Blank line -->

## Development Files

The project include the following files and folder:

1. `img`: Contains the images to display in the current `README` file.
   <br><br> <!-- Blank line -->

2. `requirements.txt`: Contains the packages and versions used to run
   the project locally (no required in Udacity's instance).
   <br><br> <!-- Blank line -->

3. `dl.cfg`: It contains the required data and credentials to run the
   project correctly. Required by `check_s3_bucket.py`,
   `create_tables.py` and `etl.py`
   <br><br> <!-- Blank line -->

4. `check_s3_bucket.py`: This script connects to the S3 bucket using an
   user with coding privileges and Administrator Access to explore the
   keys and display some JSON data samples, from here where were
   extracted the trees, JSON samples and dataframe of the section
   [Datasets](#datasets) of this README. Helpful to understand better
   the data structure.
   <br><br> <!-- Blank line -->

5. `database_handler.py`: Contains the code required to load the
   Database credentials, generate the connection and cursor instances,
   and perform the open and close connection actions. Required by
   `create_tables.py` and `etl.py`.
   <br><br> <!-- Blank line -->

6. `sql_queries.py`: Contains all the **SQL queries**. Required by
   `create_tables.py` and `etl.py`.
   <br><br> <!-- Blank line -->

7. `create_tables.py`: Drops and creates the Redshift tables. Running
   this file resets the tables before each time to run the **ETL**
   script called `etl.py`.
   <br><br> <!-- Blank line -->

8. `etl.py`: It is used to load data from S3 into staging tables on
   **Redshift** and then process that data into the analytics tables on
   Redshift, the code is designed to avoid duplicated IDs (**user_id**,
   **songplay_id**, **artist_id** and **song_id**).
   <br><br> <!-- Blank line -->

> **NOTE**: Inside each file there are the corresponding docstrings and
> comments to explain in detail the pipelines execution.
> <br><br> <!-- Blank line -->

<br><br> <!-- Blank line -->

## Setup Development Environment

> Ignore this section if the code is run inside an Udacity's instance.
> <br><br> <!-- Blank line -->

The current project is developed in **Anaconda** installed on a
**Windows 10** OS; use the corresponding Python version
(**Python v.3.9.6 64-bit**) only if the project will run locally.

- Check the Python version in the current environment using the
  following command line (it is assumed Miniconda or Anaconda is
  installed):

  `python --version`

- To verify the Python architecture installed use the following command:

  `python -c "import struct; print(struct.calcsize('P')*8)"`

  The number returned correspond to the architecture (*32* for *32-bits*
  and *64* for *64-bits*).

It is important to **have the same Python version** to reduce
incompatibilities when exporting the scripts from one environment to
other, the following sections help to create a Python Virtual
environment on case the Python version is not available in an existing
virtual environment.
<br><br> <!-- Blank line -->

### Create Environment

An environment helps to avoid packages conflicts, and it allows
developing software under similar production conditions.

_**The environment is set using Anaconda Python package distribution.**_

* Create a new environment:

  `conda create -n data_engineer python=3.9.6`

* Install pip into the new environment:

  `conda install -n data_engineer pip`

* After the environment creation, it is possible to activate it and
  deactivate it using the next commands:

  `conda activate data_engineer`

  `conda deactivate`

> NOTES:
> 
> * Get additional insights from the Anaconda Portal called
>   [Managing Environments](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#using-pip-in-an-environment).
> 
> * There is a lot of information and useful tips online; use the
>   [Conda Cheat Sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf)
>   and this
>   [Towards Article](https://towardsdatascience.com/a-guide-to-conda-environments-bc6180fc533)
>   to get additional insights about Anaconda environments.

> **WARNING!!!**:
> 
> Install the management packages using **conda** and then use **pip**
> to install the development packages, it avoids unexpected behaviors,
> never try to use **conda** installer after using for the first time
> the command **pip** to install packages.
> <br><br>
> 
<br><br> <!-- Blank line -->

### Create Jupyter Kernel Environment

* Verify the Jupyter Notebook installation in the root environment:

  `conda install jupyter`

* Activate the development environment and install the Jupyter Kernel:

  `conda activate data_engineer`

  `conda install ipykernel`

* Link the Jupyter Kernel to Jupyter Notebook:

* `python -m ipykernel install --user --name data_engineer`
  `--display-name "Python v.3.9.6 (data_engineer)"`

* Launch the Jupyter Notebook, and check the new environment is
  available as a Jupyter Kernel option.

> NOTES:
> 
> * Get more insights about Jupyter Kernel Environments in this
>   [Stack Overflow Post](https://stackoverflow.com/questions/58068818/how-to-use-jupyter-notebooks-in-a-conda-environment).
> <br><br> <!-- Blank line -->
>
<br><br> <!-- Blank line -->

### Package Requirements

The package requirements are registered in their corresponding
**requirements.txt** file, this file is created manually alongside the
development process and in the same installation order.

It helps to avoid installation errors and incompatibilities at setting
environments on different machines.

The following command lines help to list correctly the package
requirements and versionig.

* List the installed packages and versions:

  `pip freeze`

  `pip list --format=freeze`

* If the **requirements.txt** file is completed or it is required to
  replicate the environment using the current development, use the next
  command line to install again the packages due to an environment
  crashing:

  `pip install -r requirements.txt`

  If the installation does not run correctly, use the next command to
  install one-by-one the required packages and versions following the
  **requirements.txt** packages and versions:

  `pip install <package>==<version_number>`

<br><br> <!-- Blank line -->

## Setup AWS

This setup is independent of where the project is running.
<br><br> <!-- Blank line -->

### Create IAM Role

Create an **IAM Role** that will be **attached** later to the
**Redshift Cluster**, then the cluster can load data from Amazon S3
Buckets.
<br><br> <!-- Blank line -->

1. Signed into the AWS management console and navigate to the IAM
   service dashboard, this part is Global, so do not pay attention to
   the Region.
   <br/><br/> <!-- Blank line -->

2. In the left navigation pane, choose **Roles** and click in
   **Create Role**.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/IAM_role_creation.png" />
  <figcaption>Create role in IAM</figcaption>
</div>
<br/><br/> <!-- Blank line -->

3. In the **AWS Service** group as the trusted entity, and choose
   **Redshift** service. Under **Select your use case**, choose
   **Redshift - Customizable**, and then click on **Next: Permissions.**
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/redshift_customizable.png" />
  <figcaption>Make the Redshift customizable</figcaption>
</div>
<br/><br/> <!-- Blank line -->

4. On the **Attach permissions policies** page, search for and select
   the **AmazonS3ReadOnlyAccess** policy, and then click on the
   **Next: Tags** button. The next section is **Tags**, ignore this part
   and just click on the **Next: Review** button.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/s3policy.png" />
  <figcaption>Define the policies</figcaption>
</div>
<br/><br/> <!-- Blank line -->

5. For **Role name**, enter `myRedshiftRole`, and then choose
   **Create Role**.

<div align="center">
  <img width="700" src="img/name_redshift_role.png" />
  <figcaption>Give a name to the Role</figcaption>
</div>
<br/><br/> <!-- Blank line -->

6. Wait for the success message when the new role is created. 

<div align="center">
  <img width="700" src="img/redshift_role_created.png" />
</div>
<br/><br/> <!-- Blank line -->

### Create Security Group

This Security Group will **authorize access** to the
**Redshift Cluster**. A security group will act as firewall rules for
the Redshift Cluster to control inbound and outbound traffic.

1. Navigate to the
   [EC2 service](https://console.aws.amazon.com/ec2).
   
   > **WARNING!!!**: Make sure the AWS page is in the Region
   > `US WEST (Oregon) us-west-2` because the datasets are store in a
   > S3 Bucket located in the same region.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/ec2_console.png" />
  <figcaption>
    Access to EC2 console in the Region "US WESTT (Oregon) us-west-2"
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->

2. Display the **Network and Security** options from the left navigation
   panel and select **Security Groups**. Click on the
   **Create Security Group** button to launch the wizard.
   <br/><br/> <!-- Blank line -->
   
<div align="center">
  <img width="700" src="img/wizard_security_group.png" />
  <figcaption>Launch the "Create security group" wizard</figcaption>
</div>
<br/><br/> <!-- Blank line -->

3. In the **Create security group** wizard, enter the basic details. If
   something is not clear, use the
   [Create default VPC](https://docs.aws.amazon.com/vpc/latest/userguide/default-vpc.html#create-default-vpc)
   page.
   <br/><br/> <!-- Blank line -->
   
<div align="center">
  <img width="700" src="img/security_group_settings_1.png" />
  <figcaption>Table of settings to insert in the wizard</figcaption>
</div>
<br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/default_vpc.png" />
  <figcaption>Create a default VPC</figcaption>
</div>
<br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/vpf_info.png" />
  <figcaption>End the default VPC creation</figcaption>
</div>
<br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/vpc_details.png" />
  <figcaption>
    Write down the table of settings in the "Basic Details" section
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->

4. In the **Inbound rules** section, click on **Add Rule** and enter the
   following values.
   
   > **WARNING!!!** Using `0.0.0.0/0` is not recommended
   > for anything other than demonstration purposes because it allows
   > access from any computer on the internet. In a real environment, 
   > create inbound rules based on own network settings.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/vpc_rules.png" />
  <figcaption>Inbound VPC rules table</figcaption>
</div>
<br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/set_rules.png" />
  <figcaption>
    Write down the inbound VPC table in the "Inbound rules" section
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->
   
5. Outbound rules allow traffic to anywhere by default.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/traffic_vpc.png"/>
  <figcaption>Just add a description if desired</figcaption>
</div>
<br/><br/> <!-- Blank line -->

6. Click on the **Create security group** button at the bottom. Wait for
   the success message.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/security_success.png" />
  <figcaption>Security group created successfully</figcaption>
</div>
<br/><br/> <!-- Blank line -->

### Create Redshift Cluster Subnet Group

1. Navigate to the
   [Amazon Redshift Console](https://console.aws.amazon.com/redshift/).

   > **WARNING!!!**: Make sure the AWS page is in the Region
   > `US WEST (Oregon) us-west-2` because the datasets are store in a S3
   > Bucket located in the same region.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/aws_console_redshift.png" />
  <figcaption>
    Access to Redshift console in the Region "US WEST (Oregon)
    us-west-2" 
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->

2. Display the **Configurations** options from the left navigation
   panel and select **Subnet Groups**. Click on the
   **Create cluster subnet group** button to launch the wizard.
   <br/><br/> <!-- Blank line -->
   
<div align="center">
  <img width="700" src="img/default_cluster_2.png" />
  <figcaption>
    Launch the "Create cluster subnet group" wizard
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->

3. Write in the **Name** section the subnet group as
   `cluster-subnet-group-1`, add a description if desired in the
   **Cluster subnet group details** section and select the default VPC
   in the **Add subnets** section.
   <br/><br/> <!-- Blank line -->
   
<div align="center">
  <img width="700" src="img/default_cluster_3.png" />
  <figcaption>
    Set the subnet group details and VPC
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->

4. Create the subnet group and wait until the new subnet group is
   completed.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/cluster_status.png" />
  <figcaption>
    Set the subnet group details and VPC
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->

### Create Redshift Cluster

1. Navigate to the
   [Amazon Redshift Console](https://console.aws.amazon.com/redshift/).

   > **WARNINGS!!!**:
   > * This cluster after the creation will be live, and then
   >   will be charged the standard Amazon Redshift usage fees for the
   >   cluster until its deletetion. Make sure to delete the cluster each
   >   time finishing working to avoid large and unexpected costs. The 
   >   deletion instructions are available in the
   >   [Delete Redshift Cluster](#delete-redshift-cluster) section.
   > <br/><br/> <!-- Blank line -->
   > * Make sure the AWS page is in the Region
   > `US WEST (Oregon) us-west-2` because the datasets are store in a S3
   > Bucket located in the same region.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/aws_console_redshift.png" />
  <figcaption>
    Access to Redshift console in the Region "US WEST (Oregon)
    us-west-2" 
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->

2. On the Amazon Redshift Dashboard, choose **Create cluster**. It will
   launch the **Create cluster** wizard.
   <br/><br/> <!-- Blank line -->
   
<div align="center">
  <img width="700" src="img/create_cluster.png" />
  <figcaption>Launch the "Create Cluster" wizard</figcaption>
</div>
<br/><br/> <!-- Blank line -->

2. **Cluster configuration**: Provide a unique identifier, such as
   `redshift-cluster-1`, and choose the **Production** option (The
   **Free Trial** option will not display all the required options).
   Then choose the following options to set the correct Cluster
   configuration:
   * **1 node** of **dc2.large** hardware type. It is a high
     performance with fixed local SSD storage.
   * 2 vCPUs.
   * 160 GB storage capacity.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/cluster_settings.png" />
  <figcaption>
    Set the cluster settings in "Production" options
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->

3. In the **Sample data** section leave unmarked the
   **Load sample data**
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/sample_data.png" />
  <figcaption>Do not load the sample data</figcaption>
</div>
<br/><br/> <!-- Blank line -->

4. Set the **Database configurations** using the following data in the
   table, use a secure password.

<div align="center">
  <img width="700" src="img/database_config.png" />
  <figcaption>Database settings table</figcaption>
</div>
<br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/database_config_2.png" />
  <figcaption>Configure the database</figcaption>
</div>
<br/><br/> <!-- Blank line -->

5. Display the **Cluster permissions** section and choose the IAM role
   created in the section [Create IAM Role](#create-iam-role),
   **myRedshiftRole**, from the drop-down and click on the
   **Associate IAM role** button.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/cluster_permissions.png" />
  <figcaption>Assign permissions to the Redshift Cluster</figcaption>
</div>
<br/><br/> <!-- Blank line -->

6. Disable the **Additional configurations Use defaults**.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/additional_configs.png" />
  <figcaption>
    Disable the defaults settings for additional configurations
  </figcaption>
</div>
<br/><br/> <!-- Blank line -->

7. In the **Network and security** section, add to the
   **VPC security groups** the created security group from the section
   [Create Security Group](#create-security-group), then assign to the
   **Cluster subnet group**  the subnet group created from the section
   [Create Redshift Cluster Subnet Group](#create-redshift-cluster-subnet-group)
   , and finally, set the **Publicly accessible** to **Enable** .
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/network_settings.png" />
  <figcaption>Set the network and security configuration</figcaption>
</div>
<br/><br/> <!-- Blank line -->

8. At the bottom of the creation Redshif Cluster wizard, click on the
   `Create cluster` buttom. The click on the **Clusters** menu item from
   the left navigation panel, and look at the cluster launched. Make
   sure that the status is `Available` before trying to connect to the
   database. The process could take from 5 to 10 minutes.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/cluster_timing.png" />
  <figcaption>Create anc check the Redshift Cluster status</figcaption>
</div>
<br/><br/> <!-- Blank line -->

### Create IAM user with code permissions

This IAM user is created to allow a Python script access to S3 using the
AWS SDK **boto3**.

1. Access to IAM and begin the process to create a new user called
   **dwhadmin**.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/aws_create_dwhadmin_1.png" />
  <figcaption>Set the network and security configuration</figcaption>
</div>
<br/><br/> <!-- Blank line -->

2. Create a new user called **dwhadmin** and give it
   **Programmatic access**.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/aws_create_dwhadmin_2.png" />
  <figcaption>Set new user basic configurations</figcaption>
</div>
<br/><br/> <!-- Blank line -->

3. Go to the **Attach existing policies** and select the
   **AdministratorAccess** and click on the **Next: Tags** buttom.
  <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/aws_create_dwhadmin_3.png" />
  <figcaption>Give "AdministratorAccess" policy</figcaption>
</div>
<br/><br/> <!-- Blank line -->

4. Ignore the tags scetion and click on the **Next: Review** buttom.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/aws_create_dwhadmin_4.png" />
  <figcaption>Ignore Tags section</figcaption>
</div>
<br/><br/> <!-- Blank line -->

5. Make sure the policy is attached to the user and that it has the
   programmatic access, finally click on the **Create user** buttom.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/aws_create_dwhadmin_5.png" />
  <figcaption>Check the configurations and create the user</figcaption>
</div>
<br/><br/> <!-- Blank line -->

6. Keep the credentials in a secure place.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/aws_create_dwhadmin_6.png" />
  <figcaption>Save the credentials into a secure place</figcaption>
</div>
<br/><br/> <!-- Blank line -->

### Delete Redshift Cluster

> **WARNING!!!**: 
> 
> * Delete the Redshift Cluster each time at finishing working to avoid
>   large and unexpected costs. Launch a new cluster each time when
>   required.
> <br/><br/> <!-- Blank line -->
> * Make sure the AWS page is in the Region `US WEST (Oregon) us-west-2`
>   because the datasets are store in a S3 Bucket located in the same
>   region, and also this Redshift Cluster.

1. On the **Clusters** page of the Amazon Redshift console, click on the
   check-box next to the cluster name Then click on the **Actions** drop-down
   button on top → select **Delete**.
   <br/><br/> <!-- Blank line -->

<div align="center">
  <img width="700" src="img/delete_cluster_1.png" />
  <figcaption>Delete the Redshift Cluster</figcaption>
</div>
<br/><br/> <!-- Blank line -->

2. Choose to not **Create final snapshot**, and click on the
   **Delete cluster** button.
   <br/><br/> <!-- Blank line -->
   
<div align="center">
  <img width="700" src="img/delete_cluster_2.png" />
  <figcaption>Confirm Redshift Cluster deletion</figcaption>
</div>
<br/><br/> <!-- Blank line -->

3. The cluster will change its status to **deleting**, and then it will
   disappear from the Redshift Cluster list once it has finished
   deleting.
   <br/><br/> <!-- Blank line -->

# Pipeline Execution

> **NOTE**: Inside each file there are the corresponding docstrings and
> comments to explain in detail the pipelines execution.
> <br><br> <!-- Blank line -->

<br><br> <!-- Blank line -->

1. Set the correct credentials in the `dwh.cfg`, refers to the
   [Setup AWS](#setup-aws) section to generate those credentials.

2. Create the staging, dimension and fact tables running the following
   command:

   `python create_tables.py`

3. Once the process finished, run the ETL pipeline, this will shows a
   pair of metrics as example of the tables are completed and filled:

   `python etl.py`

4. It is possible to run a program just to explore the S3 Buscket where
   the datasets are stored:

   `python check_s3_bucket.py`

<br><br> <!-- Blank line -->

# Expected Results

The following images shows the final tables content after executing the
finished `etl.py` script, the images are extracted from the
**DBeaver Universal Database Manager v.7.2.5.202011152110**

## Artist Dimension Table

This table has **9,553** rows of artists data.

<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="800" src="img/artists_full.png"/>
  <figcaption>Sample from artist table</figcaption>
</div>
<br/><br/> <!-- Blank line -->

## Song Dimension Table

This table has more than **10,000** rows of song data.

<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="800" src="img/songs_full.png"/>
  <figcaption>Sample from song table</figcaption>
</div>
<br/><br/> <!-- Blank line -->

## Users Dimension Table

This table has **97** rows of user data.

<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="800" src="img/users_full.png"/>
  <figcaption>Sample from user table</figcaption>
</div>
<br/><br/> <!-- Blank line -->

## Time Dimension Table

This table has **8023** rows of records of time.

<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="800" src="img/time_full.png"/>
  <figcaption>Sample from time table</figcaption>
</div>
<br/><br/> <!-- Blank line -->

## Songplays Fact Table

This table has **6820** rows of records of songplays.

<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="800" src="img/songplays_full.png"/>
  <figcaption>Sample from songplays table</figcaption>
</div>
<br/><br/> <!-- Blank line -->

As a detail noted, the table has only **319 rows** with complete data,
to see the content just execute the following SQL statement on
**DBeaver** or in code:

```sql
SELECT *
FROM songplay s
WHERE s.song_id IS NOT NULL AND s.artist_id IS NOT NULL;
```

# Discussion

The current database schema is focused on how frequent the users use the
platform and identify which songs and artist are frequently heard.

Based on the `Sparkify` database, it is possible to retrieve different
metrics, some of them are:

1. How many users pays for the platform use? 

```sql
%SELECT u."level", COUNT(u."level")
FROM users u
GROUP BY u."level";
```

| level       | count       |
| ----------- | ----------- |
| free        | 75          |
| paid        | 22          |

The intention is users paid for the use of the platform, and the metrics
shows that less than 25% of users pay, comparing the number of paid
licenses per month could indicates if a marketing campaign is working or
not.

> **Note**: This query is also executed inside the `etl.py` pipeline
> after inserting and updating all the tables.

2. What are the most frequent hours the users use the platform?

```sql
SELECT t."hour", COUNT(t."hour") AS frequency
FROM songplay sp
JOIN "time" t
ON sp.start_time = t.start_time
GROUP BY t."hour"
ORDER BY frequency DESC
LIMIT 5;
```

Query result:
| hour        | frequency   |
| ----------- | ----------- |
| 16          | 542         |
| 18          | 498         |
| 17          | 494         |
| 15          | 477         |
| 14          | 432         |

This could help to decide at which hours put some marketing or
promotions to free users.

> **Note**: This query is also executed inside the `etl.py` pipeline
> after inserting and updating all the tables.

**About ETL pipeline**

The `ETL` pipeline could have some issues, for example, as seeing in the
image results, there a lot of missing artists and songs IDs, at this
point it is required to speak with Sparkify's responsibles, because it
is important to know how to assing those IDs, using an arbitrary ID?
waiting until new songs data files appears?

<br><br> <!-- Blank line -->




ETL
Criteria 	Meets Specifications

The etl.py script runs without errors.
	

The script, etl.py, runs in the terminal without errors. The script reads song_data and load_data from S3, transforms them to create five different tables, and writes them to partitioned parquet files in table directories on S3.

Analytics tables are correctly organized on S3.
	

Each of the five tables are written to parquet files in a separate analytics directory on S3. Each table has its own folder within the directory. Songs table files are partitioned by year and then artist. Time table files are partitioned by year and month. Songplays table files are partitioned by year and month.

The correct data is included in all tables.
	

Each table includes the right columns and data types. Duplicates are addressed where appropriate.

Code Quality
Criteria 	Meets Specifications

The project shows proper use of documentation.
	

The README file includes a summary of the project, how to run the Python scripts, and an explanation of the files in the repository. Comments are used effectively and each function has a docstring.

The project code is clean and modular.
	

Scripts have an intuitive, easy-to-follow structure with code separated into logical functions. Naming for variables and functions follows the PEP8 style guidelines.
